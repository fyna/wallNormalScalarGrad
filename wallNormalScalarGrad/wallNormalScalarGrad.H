/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013-2016 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::functionObjects::wallNormalScalarGrad

Group
    grpForcesFunctionObjects

Description
    Calculates and writes the scalar normal gradient at wall patches as
    the volScalarField field 'wallNormalScalarGrad'.

        \f[
            grad_n s
        \f]

    where
    \vartable
        n       | patch normal vector (into the domain)
    \endvartable

    Example of function object specification:
    \verbatim
    wallNormalScalarGrad1
    {
        type        wallNormalScalarGrad;
        libs        ("libfieldFunctionObjects.so");
        ...
        patches     (".*Wall");
    }
    \endverbatim

Usage
    \table
        Property | Description               | Required    | Default value
        type     | type name: wallNormalScalarGrad | yes        |
        patches  | list of patches to process | no         | all wall patches
    \endtable

Note
    Writing field 'wallNormalScalarGrad' is done by default, but it can be overridden
    by defining an empty \c objects list. For details see writeLocalObjects.

See also
    Foam::functionObject
    Foam::functionObjects::fvMeshFunctionObject
    Foam::functionObjects::logFiles
    Foam::functionObjects::writeLocalObjects
    Foam::functionObjects::pressureTools
    Foam::functionObjects::timeControl

SourceFiles
    wallNormalScalarGrad.C

\*---------------------------------------------------------------------------*/

#ifndef functionObjects_wallNormalScalarGrad_H
#define functionObjects_wallNormalScalarGrad_H

#include "fvMeshFunctionObject.H"
#include "logFiles.H"
#include "writeLocalObjects.H"
#include "volFieldsFwd.H"
#include "HashSet.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace functionObjects
{

/*---------------------------------------------------------------------------*\
                       Class wallNormalScalarGrad Declaration
\*---------------------------------------------------------------------------*/

class wallNormalScalarGrad
:
    public fvMeshFunctionObject,
    public logFiles,
    public writeLocalObjects
{

protected:

    // Protected data

        //- Optional list of patches to process
        labelHashSet patchSet_;

	word fieldName;

    // Protected Member Functions

        //- File header information
        virtual void writeFileHeader(const label i);

        //- Calculate the shear-stress
        void calcScalarGrad
        (
            const volVectorField& grdField,
            volScalarField& normalGrad
        );


private:

    // Private member functions

        //- Disallow default bitwise copy construct
        wallNormalScalarGrad(const wallNormalScalarGrad&);

        //- Disallow default bitwise assignment
        void operator=(const wallNormalScalarGrad&);


public:

    //- Runtime type information
    TypeName("wallNormalScalarGrad");


    // Constructors

        //- Construct from Time and dictionary
        wallNormalScalarGrad
        (
            const word& name,
            const Time& runTime,
            const dictionary&
        );


    //- Destructor
    virtual ~wallNormalScalarGrad();


    // Member Functions

        //- Read the wallNormalScalarGrad data
        virtual bool read(const dictionary&);

        //- Calculate the wall shear-stress
        virtual bool execute();

        //- Write the wall shear-stress
        virtual bool write();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace functionObjects
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
